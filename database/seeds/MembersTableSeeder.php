<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$path = resource_path('seed_data/');
        $members = json_decode(file_get_contents($path.'members.json'));
        foreach ($members as $m) {
	        DB::table('members')->insert([
	        	'ext_id' => $m->id, // external API id column
	        	'firstname' => $m->firstname,
	        	'surname' => $m->surname,
	        	'email' => $m->email,
	        	'gender' => $m->gender,
	        	'joined_date' => new Carbon($m->joined_date), // current format should parse fine
	        ]);
        }
    }
}
