window.Vue = require('vue');

import memberChart from './components/MemberChart.vue';
Vue.component("member-chart", memberChart);
import memberFilter from './components/MemberFilter.vue';
Vue.component("member-filter", memberFilter);

import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

window.app = new Vue({
    el: '#app',
    components: {Line}
});