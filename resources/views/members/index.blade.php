@extends('layouts.app')

@section('title', 'Member Search')

@section('content')
    <h1 class="mt-5">Members</h1>

    <member-filter :last_input="{{ json_encode(request()->input()) }}"></member-filter>

    <member-chart :sign_ups="{{ json_encode($sign_ups) }}"></member-chart>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Email</th>
                <th scope="col">Gender</th>
                <th scope="col">Joined</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($members as $member)
                <tr>
                  <th scope="row">{{ $member->ext_id }}</th>
                  <td>{{ $member->firstname }}</td>
                  <td>{{ $member->surname }}</td>
                  <td>{{ $member->email }}</td>
                  <td>{{ $member->gender }}</td>
                  <td>{{ $member->joined_date->format('d-m-Y') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $members->appends(request()->input())->links() }}
@endsection
