<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Laravel App - @yield('title')</title>
<link href="/css/app.css" rel="stylesheet">