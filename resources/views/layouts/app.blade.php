<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.partials.head')
    </head>
    <body class="flex-shrink-0">
        <div class="container" id="app">
            @yield('content')
        </div>

        @include('layouts.partials.footer-scripts')
    </body>
</html>


