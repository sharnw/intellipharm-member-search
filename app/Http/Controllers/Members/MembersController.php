<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Members;
use Cache;
use DB;

class MembersController extends Controller
{

    /**
     * Shows paginated member list with with filtering options.
     * 
     * Also displays a chart of customer sign-ups per year.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $page = $request->has('page') ? $request->query('page') : 1;

        $cacheKey = "members::page={$page}";
        $whereParams = [];
        if ($request->has('firstname') && $request->firstname != "") {
            $cacheKey .= "::firstname={$request->firstname}";
            $whereParams[] = ['firstname', "{$request->firstname}"];
        }
        if ($request->has('surname') && $request->surname != "") {
            $cacheKey .= "::surname={$request->surname}";
            $whereParams[] = ['surname', "{$request->surname}"];
        }
        if ($request->has('email') && $request->email != "") {
            $cacheKey .= "::email={$request->email}";
            $whereParams[] = ['email', "{$request->email}"];
        }

        // cache members query for 60 minutes
        $members = Cache::remember($cacheKey, 60, function() use ($whereParams) {
            return Members::where($whereParams)
                ->orderBy('firstname', 'asc', 'lastname', 'asc', 'email', 'asc')
                ->paginate(15);
        });

        // cache sign_ups query for 300 minutes
        $sign_ups = Cache::remember('sign_ups', 300, function() {
            return Members::select(DB::raw('COUNT(id) as `sign_ups`'),  DB::raw('YEAR(joined_date) as year'))
                ->groupby('year')
                ->get();
        });

        return view('members.index', compact('members', 'sign_ups'));
    }
}
