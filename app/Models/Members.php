<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $fillable = [
        'ext_id',
        'firstname',
        'surname',
        'email',
        'gender',
        'joined_date'
    ];
    protected $dates = ['joined_date'];
}
