## Installation Requirements

* Please make sure that all requirements for Laravel 5.8 have been met [Laravel Server Requirements](https://laravel.com/docs/5.8/installation#server-requirements)
* You will need MySQL 5.7 or later and a freshly created database.
* You will need an instance of redis. 
* You will need to have Node and NPM installed.

## Installation Guide

1. Clone the repo and cd to corresponding folder.
2. Enter the configuration details of your MySQL & Redis instances in the .env file (including ``DB_DATABASE=freshly_createdb_db_name``)
3. Run: ``composer install``
4. Run: ``npm install``
5. Run: ``npm run production``
6. Run: ``php artisan migrate``
7. Run: ``php artisan db:seed``

## Dev Notes

* I chose to use laravel purely because i have laravel valet running and it saved me a lot of time setting up the dev environment.
* The customer sign-ups chart doesn't change when the member list filters change (intentional). 
* I did not receive the members.sql file so instead i based my tables on the fields retrieved from: https://intellipharm.com.au/devtest/index.php.
* Ran out of time for the drill-down of month/year for customer sign-ups.

## Screenshots

* [Displaying full member list with pagination - 67% scale](https://i.imgur.com/0NPHG8h.png)